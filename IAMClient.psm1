$script:IAMCreds = $null
$script:ApiHost = "https://iam.password.ethz.ch/iam-ws-legacy"
$script:DebugMode = $false

$Public = @( Get-ChildItem -Path "$PSScriptRoot\Public" -Include "*.ps1" -Recurse -ErrorAction SilentlyContinue )
$Private = @( Get-ChildItem -Path "$PSScriptRoot\Private" -Include "*.ps1" -Recurse -ErrorAction SilentlyContinue )


#Dot source the files
foreach ($import in @($Public + $Private)) {
    Try {
        . $import.FullName
    }
    Catch {
        Write-Error -Message "Failed to import function $($import.fullname): $_"
    }
}


Export-ModuleMember -Function $Public.basename