# IAM PowerShell Module

The IAM PowerShell Module simplifies access to the IAM API.

It can be used to automate a lot of tasks for IAM.

## Installation

Installation is pretty straightforward, as it's available on the PowerShell Gallery.

```powershell
Install-Module -Name "IAMClient"
```

or alternatively, download the latest release from the [Releases](https://gitlab.ethz.ch/aurels/iam-powershell/releases) page and import it yourself

## Examples

There are a few example scripts to help you get going:
**[Examples](examples/)**

## Cmdlet Groups

The Module is divided into the following Groups that mostly concern CRUD (Create Retrieve Update Delete).

For examples and help, refer to the individual group help pages.

- [User](public/ethuser/) - Work with users
- [Person](public/ethperson/) - Work with people
- [Group](public/ethgroup/) - Working with groups
- [Maillist](public/ethmaillist/) - Work with maillists
- *[Connection](public/connection/) - Connecting to the IAM webservice*

---

## Compilation

Every *public* cmdlet resides in it's own file. If you want to only work with one simple file, head over to the [releases](https://gitlab.ethz.ch/aurels/iam-powershell/releases) section and download a zip of the newest 'compiled' version, whichof you can extract the .psm1-file (IAMClient.psm1) for easy import.

### Compile yourself

Run the `compile.ps1` script in the [build](build/) directory. The compiled module file together with all files will be in the root folder `dist` together with a .zip archive of all files.