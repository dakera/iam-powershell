---
external help file: IAMClient-help.xml
Module Name: IAMClient
online version:
schema: 2.0.0
---

# Get-ETHUserGroupMembership

## SYNOPSIS
Gets all memberships of the given user

## SYNTAX

```
Get-ETHUserGroupMembership [-Identity] <String> [<CommonParameters>]
```

## DESCRIPTION
This will load all group memberships with type "Custom","Admin" and "Netsup" for the given user(s)

## EXAMPLES

### EXAMPLE 1
```
Get-ETHUserGroupMembership aurels
```

### EXAMPLE 2
```
"aurels","" Get-ETHUserGroupMembership aurels
```

## PARAMETERS

### -Identity
The username to find

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: True (ByPropertyName, ByValue)
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see [about_CommonParameters](http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
