---
external help file: IAMClient-help.xml
Module Name: IAMClient
online version:
schema: 2.0.0
---

# Sync-ETHMaillistMember

## SYNOPSIS
Synchronizes members mailinglists (DEPRECATED)

## SYNTAX

```
Sync-ETHMaillistMember [-ReferenceLists] <String[]> [-SyncList] <String> [-AllowADFallback]
 [<CommonParameters>]
```

## DESCRIPTION
Sync-ETHMaillist members can be used to emulate nesting without actually nesting groups
The members can be synchronized from other groups, f.ex OC-groups and no_oc-Groups

## EXAMPLES

### EXAMPLE 1
```
Sync-ETHMaillistMember -ReferenceLists "biol-micro-test-list" -SyncList "biol-micro-list-aebi"
```

This copies all members from the list "biol-micro-test-list" to the list "biol-micro-list-aebi"

### EXAMPLE 2
```
Sync-ETHMaillistMember -ReferenceLists "biol-micro-test-list","biol-isg-all" -SyncList "biol-micro-list-aebi"
```

This does the same as example 1, but with multiple sourcelists which will be combined

## PARAMETERS

### -ReferenceLists
The lists to get the members **from**

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -SyncList
The destination list that will be **modified**

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: True
Position: 2
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -AllowADFallback
Falls back to AD if group cannot be loaded via IAM

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: 3
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see [about_CommonParameters](http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS

[Set-ETHMaillistMember
Set-ETHGroupMember
Sync-ETHGroupMember]()

