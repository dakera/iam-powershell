---
Module Name: IAMClient
Module Guid: 33ce3afe-9156-4f0e-bbc7-6d4fab3f2ad7
Download Help Link: {{ Update Download Link }}
Help Version: {{ Please enter version of help manually (X.X.X.X) format }}
Locale: en-US
---

# IAMClient Module
## Description
{{ Fill in the Description }}

## IAMClient Cmdlets
### [Add-ETHGroupMember](Add-ETHGroupMember.md)
{{ Fill in the Synopsis }}

### [Add-ETHMaillistMember](Add-ETHMaillistMember.md)
{{ Fill in the Synopsis }}

### [Add-ETHUserITService](Add-ETHUserITService.md)
{{ Fill in the Synopsis }}

### [Clear-ETHMaillistMember](Clear-ETHMaillistMember.md)
{{ Fill in the Synopsis }}

### [Get-ETHGroup](Get-ETHGroup.md)
{{ Fill in the Synopsis }}

### [Get-ETHGroupMember](Get-ETHGroupMember.md)
{{ Fill in the Synopsis }}

### [Get-ETHMaillist](Get-ETHMaillist.md)
{{ Fill in the Synopsis }}

### [Get-ETHMaillistMember](Get-ETHMaillistMember.md)
{{ Fill in the Synopsis }}

### [Get-ETHPerson](Get-ETHPerson.md)
{{ Fill in the Synopsis }}

### [Get-ETHPersonServices](Get-ETHPersonServices.md)
{{ Fill in the Synopsis }}

### [Get-ETHUser](Get-ETHUser.md)
Gets the parameters of a IT-Service for a user (Similar to Get-ADUser)

### [Get-ETHUserGroupMembership](Get-ETHUserGroupMembership.md)
Gets all memberships of the given user

### [Initialize-IAMClient](Initialize-IAMClient.md)
{{ Fill in the Synopsis }}

### [Invoke-IAMMethod](Invoke-IAMMethod.md)
{{ Fill in the Synopsis }}

### [New-ETHPersona](New-ETHPersona.md)
{{ Fill in the Synopsis }}

### [Remove-ETHGroupMember](Remove-ETHGroupMember.md)
{{ Fill in the Synopsis }}

### [Remove-ETHMaillist](Remove-ETHMaillist.md)
{{ Fill in the Synopsis }}

### [Remove-ETHMaillistMember](Remove-ETHMaillistMember.md)
{{ Fill in the Synopsis }}

### [Reset-ETHUserPassword](Reset-ETHUserPassword.md)
{{ Fill in the Synopsis }}

### [Set-ETHGroupMember](Set-ETHGroupMember.md)
Sets the members of an ETH group to the specified member list

### [Set-ETHMaillistMembers](Set-ETHMaillistMembers.md)
{{ Fill in the Synopsis }}

### [Set-ETHUser](Set-ETHUser.md)
{{ Fill in the Synopsis }}

### [Set-ETHUserITService](Set-ETHUserITService.md)
{{ Fill in the Synopsis }}

### [Sync-ETHGroupMember](Sync-ETHGroupMember.md)
Synchronizes users from multiple groups and mailing lists to a group and a mailinglist

### [Sync-ETHMaillistMember](Sync-ETHMaillistMember.md)
Synchronizes members mailinglists (DEPRECATED)

### [Test-ETHCredentials](Test-ETHCredentials.md)
{{ Fill in the Synopsis }}

