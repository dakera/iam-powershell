---
external help file: IAMClient-help.xml
Module Name: IAMClient
online version:
schema: 2.0.0
---

# Set-ETHGroupMember

## SYNOPSIS
Sets the members of an ETH group to the specified member list

## SYNTAX

```
Set-ETHGroupMember [-Identity] <String> [-Members] <String[]> [-WhatIf] [-Confirm] [<CommonParameters>]
```

## DESCRIPTION
Removes / Adds members to the given group until the memberlist is equal to the one submitted
You can specify either usernames or *custom* groups

## EXAMPLES

### EXAMPLE 1
```
Set-ETHGroupMember -Identity biol-micro-isg -Members @("aurels","ausc")
```

### EXAMPLE 2
```
Set-ETHGroupMember -Identity biol-micro-isg -Members @("biol-micro-isg-sadm","aurels")
```

Added:   {"aurels", "ausc"}
Removed: {}
Kept:    {}

## PARAMETERS

### -Identity
The group to edit

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Members
The list of members to set the group memberlist to

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: True
Position: 2
Default value: None
Accept pipeline input: True (ByPropertyName, ByValue)
Accept wildcard characters: False
```

### -WhatIf
Shows what would happen if the cmdlet runs.
The cmdlet is not run.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases: wi

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Confirm
Prompts you for confirmation before running the cmdlet.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases: cf

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see [about_CommonParameters](http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

## OUTPUTS

### pscustomobject. Returns a custom object with 3 properties Added, Removed and Kept to show what the cmdlet did
## NOTES

## RELATED LINKS
