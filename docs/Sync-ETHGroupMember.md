---
external help file: IAMClient-help.xml
Module Name: IAMClient
online version:
schema: 2.0.0
---

# Sync-ETHGroupMember

## SYNOPSIS
Synchronizes users from multiple groups and mailing lists to a group and a mailinglist

## SYNTAX

### ToGroup (Default)
```
Sync-ETHGroupMember [[-SourceGroups] <String[]>] [[-SourceLists] <String[]>] [-DestGroup] <String>
 [-AllowADFallback] [-WhatIf] [-Confirm] [<CommonParameters>]
```

### ToBoth
```
Sync-ETHGroupMember [[-SourceGroups] <String[]>] [[-SourceLists] <String[]>] [-DestGroup] <String>
 -DestList <String> [-AllowADFallback] [-WhatIf] [-Confirm] [<CommonParameters>]
```

### ToList
```
Sync-ETHGroupMember [[-SourceGroups] <String[]>] [[-SourceLists] <String[]>] -DestList <String>
 [-AllowADFallback] [-WhatIf] [-Confirm] [<CommonParameters>]
```

## DESCRIPTION
Copies all **users** from the source groups/lists to the given destination group/list

## EXAMPLES

### EXAMPLE 1
```
Sync-ETHGroupMember -SourceGroups "biol-micro-isg" -DestList "MICRO_IT_STAFF"
```

Copies all members from the group "biol-micro-isg" to the Maillinglist "MICRO_IT_STAFF"

### EXAMPLE 2
```
Sync-ETHGroupMember -SourceLists "MICRO_IT_STAFF","MICRO_AD_STAFF" -DestList "MICRO_STAFF"
```

Copies all members from the source lists to the destination list

### EXAMPLE 3
```
Sync-ETHGroupMember -SourceLists "MICRO_IT_STAFF","MICRO_AD_STAFF" -SourceGroups "biol-micro-institute" -DestGroup "biol-micro-institute"
```

Adds all members from the given lists to the destination group without removing users

## PARAMETERS

### -SourceGroups
The list of groups to read members from

```yaml
Type: String[]
Parameter Sets: (All)
Aliases: ReferenceGroups

Required: False
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -SourceLists
The list of mailinglists to read members from

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: False
Position: 2
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -DestGroup
The destination group that will be set to all members from the given groups / lists

```yaml
Type: String
Parameter Sets: ToGroup, ToBoth
Aliases: SyncGroup

Required: True
Position: 3
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -DestList
The destination mailinglist that will be set to all members from the given groups / lists

```yaml
Type: String
Parameter Sets: ToBoth, ToList
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -AllowADFallback
Falls back to AD if group cannot be loaded via IAM

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -WhatIf
Shows what would happen if the cmdlet runs.
The cmdlet is not run.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases: wi

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Confirm
Prompts you for confirmation before running the cmdlet.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases: cf

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see [about_CommonParameters](http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

## OUTPUTS

### System.Collections.Hashtable
### A hashtable with a report on each group/list that was modified and what was done (Add / Remove / Keep Members)
## NOTES

## RELATED LINKS
