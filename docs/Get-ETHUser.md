---
external help file: IAMClient-help.xml
Module Name: IAMClient
online version:
schema: 2.0.0
---

# Get-ETHUser

## SYNOPSIS
Gets the parameters of a IT-Service for a user (Similar to Get-ADUser)

## SYNTAX

```
Get-ETHUser [-Identity] <String> [[-Service] <String>] [<CommonParameters>]
```

## DESCRIPTION
Gets all parameters for a given service from IAM for the given user
Default is the "Mailbox" service

## EXAMPLES

### EXAMPLE 1
```
Get-ETHUser aurels
```

### EXAMPLE 2
```
Get-ETHUser aurels -Service LDAP
```

## PARAMETERS

### -Identity
The username to find

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Service
The service name to get the parameters for

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 2
Default value: Mailbox
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see [about_CommonParameters](http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

## OUTPUTS

## NOTES

## RELATED LINKS
