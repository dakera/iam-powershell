import-module "$PSScriptRoot\IAMClient.psd1"

function Main() {

    $PwdPolicy = Get-PasswordPolicy -MinLength 12 -MaxLength 14 -NoSymbols

    # Only ask for credentials if not already asked
    if (-not $AdminUser) {
        $AdminUser = Get-Credential -Message "Enter 4-EA Credentials"
    }

    # Initialize IAM Client
    try {
        Initialize-IAMClient -Credentials $AdminUser
    } catch {
        Write-Host $_
        return
    }

    <# GENERATE USERS

    $Users = New-CourseUsers -StartNumber 77 -NumberOfUsers 13 -PwdPolicy $PwdPolicy -UserPrefix "biolcourse-" -UserComment "Biol Course" -AdminUser $AdminUser -ParentPersona "ti12684"
        -HomeDrive $null -HomeDirectory $null -ProfilePath $null
    return $Users #>

    <# SET PASSWORD #>

    $Usernames = 72..83 | ForEach-Object {"biolcourse-$($_.ToString("00"))"}

        
    $Ok = @()
    $i = 0
    foreach ($User in $Usernames){
        Write-Progress -Activity "Resetting Passwords" -PercentComplete (100/ $Usernames.Length * $i) -CurrentOperation $User

        Write-Information $User
        try {
            $Result = $User | Reset-CourseUserPassword -PwdPolicy $PwdPolicy -ServiceName "Mailbox"
            $Result.Password = $Result.Password
            $OK += $Result
        } catch {
            Write-Host "$User ... $($Result.Password) ... $($Result.Status)"
        }

        $i++
    }

    Write-Progress -Activity "Resetting Passwords" -Completed #>

    return $Ok
    
}

function New-CourseUsers {

    param (
        [Parameter(Mandatory = $true)]
        [int]$NumberOfUsers,

        [Parameter()]
        [int]$StartNumber = 1,

        # The persona that will be used as parent for the usernames
        [Parameter(Mandatory = $true)]
        [string]$ParentPersona,

        [Parameter(Mandatory = $true)]
        [string]$UserPrefix,

        [Parameter(Mandatory = $true)]
        [PasswordPolicy]$PwdPolicy,

        [Parameter(Mandatory = $true)]
        [string]$UserComment,

        [Parameter(Mandatory = $true)]
        [pscredential]$AdminUser,

        [string]$HomeDrive,
        [string]$HomeDirectory,
        [string]$ProfilePath

    )

    # So that the execution stops during an error (try / catch work with this set)
    $ErrorActionPreference = "Stop"

    #
    # Create a number template, so that all usernames are the same length
    # example: biolcourse-001, biolcourse-101
    $UserNumberTemplateLength = "$($StartNumber + $NumberOfUsers -1)".Length
    $UserNumberTemplate = "0" * $UserNumberTemplateLength

    # Array for storing the users in the end
    $Users = @()

    $EndNumber = $StartNumber + $NumberOfUsers
    for ($i = $StartNumber; $i -le $EndNumber; $i++) {

        # Generate username and password
        $NewUserName = $UserPrefix + $i.ToString($UserNumberTemplate)
        $NewPassword = Get-RandomPassword -Policy $PwdPolicy

        # Specify Mailbox parameters
        $MailboxBody = [PSCustomObject]@{
            sn            = $NewUserName;
            givenName     = "Service";
            homeDrive     = $HomeDrive;
            homeDirectory = $HomePath;
            profilePath   = $ProfilePath;
            enabled = 1;
        }

        # Create user or modify?
        try {
            # Try to load person
            $Persona = Get-ETHPersonServices -Identity $NewUserName
            $CreatePersona = $false
            $CreateText = "Updating Users"
        } catch {
            $CreatePersona = $true
            $CreateText = "Creating users"
        }

        # Write progress bar
        Write-Progress $CreateText -Status "$i of $EndNumber" -PercentComplete (100 / $EndNumber * $i)
        
        try {
            # Create UNAME
            if ($CreatePersona){
                New-ETHPersona -ParentIdentity $ParentPersona -UserComment $UserComment -NewUserName $NewUserName -InitPwd $NewPassword
            }

            # Add nethz service
            if ($CreatePersona){
                Add-ETHUserITService -Identity $NewUserName -ITServiceName "LDAP" -Body @{} -ea Continue
            }

            # Add Mailbox service
            if ($CreatePersona){
                Add-ETHUserITService -Identity $NewUserName -ITServiceName "Mailbox" -Body @{} #$MailboxBody
            }

            # Set password
            Reset-ETHUserPassword -Identity $NewUserName -NewPassword ($NewPassword | ConvertTo-SecureString -AsPlainText -Force) -ServiceName "Mailbox"
        }
        catch {
            # Convert JSON message
            $ErrorObject = $_.Exception.Message

            # Check if failed password
            if ($ErrorObject.status -eq 401) {
                throw "Your Credentials did not work!"
                exit
            }

            # Correct credentials, but something else failed, continue with next user
            Write-Error "Failed to Create User $NewUserName, Error: $($_.Exception.Message)"
            continue
        }

        # Successful, save User with password
        $Users += $NewUser

    }

    # Hide progress bar
    Write-Progress "Creating users" -Completed

    # Return all created users
    return $Users
}

function Reset-CourseUserPassword {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline=$true, Mandatory = $true)]
        [string]$Username,

        [Parameter(Mandatory = $true)]
        [PasswordPolicy]$PwdPolicy,

        [Parameter(Mandatory = $true)]
        [string]$ServiceName,

        # Optional Password for the user, is generated if not given
        [securestring]$NewPassword
    )

    BEGIN {
    }

    PROCESS {
        if (-not $NewPassword){
            $PlainText = Get-RandomPassword -Policy $PwdPolicy
            $NewPassword = $PlainText | ConvertTo-SecureString -AsPlainText -Force
        }

        try {
            $Status = Reset-ETHUserPassword -Identity $Username -NewPassword $NewPassword -ServiceName $ServiceName -ErrorAction "Stop"
        } catch {
            $Status = $_
        }
        [PSCustomObject]@{User = $Username; Password = $PlainText; Status = $Status}
    }
}

# This class is used to generate passwords according to this policy
class PasswordPolicy {
    [int]$MinLength
    [int]$MaxLength
    [switch]$NoNumbers
    [switch]$NoSymbols
    [string]$AllowSymbolsList
}

function Get-PasswordPolicy {

    param (
        [Parameter(Mandatory = $true)]
        [int]$MinLength,
        [Parameter(Mandatory = $true)]
        [int]$MaxLength,
        [switch]$NoNumbers,
        [switch]$NoSymbols,
        [string]$AllowSymbolsList = ".,-_+"
    )

    $Policy = New-Object PasswordPolicy
    $Policy.MinLength = $MinLength
    $Policy.MaxLength = $MaxLength
    $Policy.NoNumbers = $NoNumbers.IsPresent
    $Policy.NoSymbols = $NoSymbols.IsPresent
    $Policy.AllowSymbolsList = $AllowSymbolsList

    return $Policy

}

function Get-RandomPassword {
    param (
        [Parameter(Position = 0, Mandatory = $true)]
        [PasswordPolicy]$Policy
    )

    # Get a random Length of a password
    $PasswordLength = Get-Random -Minimum $Policy.MinLength -Maximum $Policy.MaxLength

    $tries = 0

    do {
        $tries++
        # Build a regex to validate the generated password
        $Regex = "("

        # Start with a empty string
        $Password = ""

        # Set List of CharIndexes to use
        $RandomCharInts = [System.Collections.Generic.List[int]]::new()
        
        # Add a-z and A-Z
        $RandomCharInts.AddRange([int[]](65..90)) # A-Z
        $Regex += "(?=.*[A-Z])"

        $RandomCharInts.AddRange([int[]](97..122)) # a-z
        $Regex += "(?=.*[a-z])"

        # Add Numbers
        if (-not $Policy.NoNumbers) {
            $RandomCharInts.AddRange([int[]](48..57)) # 0-9
            $Regex += "(?=.*[0-9])"
        }

        # Add Symbols
        if (-not $Policy.NoSymbols) {
            $Policy.AllowSymbolsList.ToCharArray() | ForEach-Object {$RandomCharInts.Add([int]$_)} # Add all Charindexes of specified symbols
            # escape "-" because it has special meaning in this part of the regex
            $Regex += "(?=.*[" + ($Policy.AllowSymbolsList -replace "-","\-" -replace "]","\]") + "])"
        }

        $Regex += ").{$($Policy.MinLength),$($Policy.MaxLength)}"

        for ($i = 1; $i -le $PasswordLength; $i++) {
            # Append one random character from our list
            $Password += [char]($RandomCharInts | Get-Random)
        }

    } while ($Password -notmatch $Regex)

    return $Password
}

function Convert-SecureStringToText {

    param (
        [Parameter(ValueFromPipeline = $true, Position = 0, Mandatory = $true)]
        [securestring]$InputObject
    )

    BEGIN {}

    PROCESS {
        $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($InputObject)
        return ([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR))
    }
}

# Execute the Main function
$returnVal = Main
$returnVal