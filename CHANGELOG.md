# Changelog of IAMClient

## Version 1.1.0

### Breaking Changes

- `Get-ETHUserGroupMembership` does not return the root object "groups" anymore (see 5e940a9c)

### Bugfixes

- `Get-ETHUserGroupMembership` implemented correctly (see 5e940a9c)

### Other remarks

- Changelog now in place for this and future releases
- Added new PSType `ETHZ.ID.IAMClient.IAMGroupMembership` to output of `Get-ETHUserGroupMembership` (see 7c7f33ac)