Import-Module pester
if (! $Credentials){
    $Credentials = Get-Credential -Message "Enter your 4ea credentials"
}

$Globals = @{
    TestUser = "biolcourse-71";
    TestUserSn = "Schwitter";
    TestGroup = "biol-micro-api-perm_test_2";
    TestGroup2 = "biol-micro-sunagawa-server-adm"; # just a second group, only reads are performed to this group
}

Describe "Importing the module" {
    It 'Should import' {

        # remove if loaded
        if (Get-Module IAMClient){
            Remove-Module IAMClient
        }

        {import-module ..\IAMClient.psm1} | Should -not -Throw
    }
}

Describe "Running non-initialized" {
    <#it "Should allow reading an API" {
        Get-ETHUser -Identity "aurels" | Select-Object -ExpandProperty sn | Should -be "Schwitter"
    }#>

    it "Should not allow interacting with an API" {
        {Get-ETHUser -Identity $Globals.TestUser } | Should -Throw "Please initialize the client to use this function" 
        <#$User.displayName = $user.displayName + " tst"
        {$User | Set-ETHUser -Identity $Globals.TestUser} | should -Throw "Please initialize the client to use this function"#>
    }
}

Describe "Initializing the module" {
    it "Should be initialized" {
        {Initialize-IAMClient -Credentials $Credentials} | should -not -throw
    }
}

Describe "Interacting with users" {
    It "Should load a user" {
        Get-ETHUser -Identity $Globals.TestUser | Select-Object -Expand sn | Should -be $Globals.TestUserSn
    }

    it "Should load the group memberships of a user" {
        @(Get-ETHUserGroupMembership -Identity $Globals.TestUser).Count | Should -BeGreaterThan 0
    }

    it "Should modify a user" {
        $User = Get-ETHUser $Globals.TestUser

        $SavedDescription = $User.description
        $User.description = $SavedDescription + "Test"
        {Set-ETHUser -Identity $Globals.TestUser -User $User} | should -not -Throw

        # check if change succeeded
        Get-ETHUser $Globals.TestUser | Select-Object -expand description | Should -be ($SavedDescription + "Test")

        # Reset description
        $user.description = $SavedDescription
        {Set-ETHUser -Identity $Globals.TestUser -User $User} | should -not -Throw
    }
}

Describe "Interacting with groups" {
    it "Should load a group" {
        Get-ETHGroup -Identity $Globals.TestGroup2 | Select-Object -expand name | Should -be $Globals.TestGroup2 # Obviously
    }

    it "Should add a member to a group" {
        $GroupMemberCount = @(Get-ETHGroup -Identity $Globals.TestGroup | Select-Object -Expand members).Count

        {Add-ETHGroupMember -Identity $Globals.TestGroup -Members $Globals.TestUser} | Should -not -Throw

        @(Get-ETHGroup -Identity $Globals.TestGroup | Select-Object -Expand members).Count | Should -be ($GroupMemberCount + 1)
    }

    it "Should remove a member from a group" {
        $GroupMemberCount = @(Get-ETHGroup -Identity $Globals.TestGroup | Select-Object -Expand members).Count

        if ($GroupMemberCount -eq 0){
            Set-ItResult -Skipped -Because "cannot remove a member from an empty group"
            return
        }

        {Remove-ETHGroupMember -Identity $Globals.TestGroup -Members $Globals.TestUser} | Should -not -Throw

        @(Get-ETHGroup -Identity $Globals.TestGroup | Select-Object -Expand members).Count | Should -be ($GroupMemberCount - 1)
    }

    it "Should export a group to AD" {
        Set-ItResult -skipped -Because "exporting is not needed with IAM"
        return

        if ((Get-ETHGroup -Identity $Globals.TestGroup).targets -notcontains "AD"){
            Set-ItResult -Skipped -Because "it cannot be exported to AD because it is not exported to AD"
            return
        }

        {Start-GroupProvisioning -Identity $Globals.TestGroup -AD} | should -not -Throw
    }
}