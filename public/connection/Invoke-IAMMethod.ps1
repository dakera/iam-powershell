
Function Invoke-IAMMethod {
    [CmdletBinding(SupportsShouldProcess = 1)]
    param (
        [Parameter(Position = 0, Mandatory = 1)]
        [string]$Url,

        [Parameter(Mandatory = 1)]
        [Microsoft.PowerShell.Commands.WebRequestMethod]$Method,

        [Parameter(Position = 1)]
        [psobject]$Body = "",
        
        [Parameter(Position = 2)]
        [pscredential]$Credentials

    )

    BEGIN {

        $Headers = @{ }

        If ($Credentials -ne $null) {
            $AuthHeader = "Basic " + [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("$($Credentials.UserName):$($Credentials.GetNetworkCredential().Password)"));
            # only set auth header when needed
            $Headers.Add("Authorization", $AuthHeader);
        }

        if ($Body -ne "") {
            if ($Body -is [Array]) {
                $JsonBody = ConvertTo-Json @($Body) -Compress
            }
            else {
                $JsonBody = ConvertTo-Json $Body -Compress
            }
        }
        else {
            $JsonBody = ""
        }

        # Accept header
        $Headers.Add("Accept", "application/json");

        if (-not [string]::IsNullOrWhiteSpace($JsonBody)) {
            $Headers.Add("Content-type", "application/json; charset=utf-8");
        }

        # Form complete URL and parse it
        $Uri = $script:ApiHost + $Url
        if (-not [uri]::IsWellFormedUriString($Uri, "Absolute")) {
            throw "Could not parse URI $Uri"
            return
        }

    }

    PROCESS {
        if ($PSCmdlet.ShouldProcess($Url)) {
            try {
                # only provide the bdoy when needed, as it gives an error when used with GET
                if ($Method -eq "Get") {
                    $Response = Invoke-RestMethod -Uri $Uri -Method $Method -Headers $Headers
                }
                else {
                    $Response = Invoke-RestMethod -Uri $Uri -Method $Method -Headers $Headers -Body $JsonBody
                }

                # Only write to console if debug is enabled
                if ($script:DebugMode) {
                    Write-RequestToConsole -Method $Method.ToString() -Headers $Headers -JsonBody $JsonBody
                    Write-ResponseToConsole -Response $Response
                }
            }
            catch {

                # Only write to console if debug is enabled
                if ($script:DebugMode) {
                    Write-RequestToConsole -Method $Method.ToString() -Headers $Headers -JsonBody $JsonBody
                    Write-ResponseToConsole -Response $Response
                }
                throw "API Request failed. Message: $_"
            }
        }
    }

    END {
        return $Response
    }
}