function Reset-ETHUserPassword {
    param (
        [Parameter(Position = 0, Mandatory = 1)]
        [string]$Identity,

        # Parameter help description
        [Parameter(Position = 1, Mandatory = 1)]
        [securestring]$NewPassword,

        [Parameter(Position = 2, Mandatory = 1)]
        [string]$ServiceName
    )

    BEGIN {
        $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($NewPassword)
        $PlainText = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
    }

    PROCESS {

        try {
            $result = Invoke-IAMMethod -Url "/usermgr/user/$Identity/service/$ServiceName/password" -Body @{password = $PlainText } -Credentials $script:IAMCreds -Method Put
            return $result;
        }
        catch {
            Write-Error "Could not reset the password of the user $Identity, Error: $_"
            return;
        }
    }
}