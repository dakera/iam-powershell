function Add-ETHUserITService {
    param (
        # ETH user name
        [Parameter(Position = 0, Mandatory = 1)]
        [string]
        $Identity,

        # IT Service Name
        [Parameter(Position = 1, Mandatory = 1)]
        [string]
        $ITServiceName,

        # Body
        [Parameter(Position = 2)]
        [psobject]
        $Body
    )

    BEGIN {
        # Check if client is initialized
        Test-IsIAMClientInitialized | Out-Null

        if ($Body -eq $null) {
            $Body = @{ }
        }
    }

    PROCESS {
        return (Invoke-IAMMethod -Url "/usermgr/user/$Identity/service/$ITServiceName" -Method POST -Credentials $script:IAMCreds -Body $Body)
    }
}