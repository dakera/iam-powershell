
function Set-ETHUser {
    [CmdletBinding()]
    param (
        [Parameter(Position = 0, Mandatory = 1)]
        [string]$Identity,
        [Parameter(Position = 1, Mandatory = 1, ValueFromPipeline = 1)]
        [psobject]$User,
        [Parameter(Position = 2, Mandatory = 0)]
        [string]$Service = "Mailbox"
    )

    BEGIN {
        # Check if client is initialized
        Test-IsIAMClientInitialized | Out-Null

        $sourceUser = Get-ETHUser -Identity $Identity
        $changedProperties = Get-ObjectDiffs $sourceUser $User
    }

    PROCESS {
        $result = Invoke-IAMMethod -Url "/usermgr/user/$Identity/service/$Service" -Method Put -Body $changedProperties -Credentials $script:IAMCreds
    }

    END {
        $result
        return (Get-ETHUser -Identity $Identity)
    }
}