function Get-ETHUserGroupMembership {
    <#
.SYNOPSIS
    Gets all memberships of the given user
    
.DESCRIPTION
    This will load all group memberships with type "Custom","Admin" and "Netsup" for the given user(s)

.PARAMETER Identity
    The username to find

.EXAMPLE
    Get-ETHUserGroupMembership aurels

.EXAMPLE
    "aurels","" Get-ETHUserGroupMembership aurels

#>
    [CmdletBinding()]
    param (
        [Parameter(Position = 0, Mandatory = 1, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = "Name")]
        [string]$Identity
    )

    BEGIN {
        # Check if client is initialized
        Test-IsIAMClientInitialized | Out-Null
        
    }

    PROCESS {
        $url = "/groupmgr/user/$Identity"

        if (-not (Get-ETHUser $Identity)) {
            throw "User $Identity could not be found"
        }

        $groups = (Invoke-IAMMethod -Url $url -Method Get -Credentials $script:IAMCreds).Groups
        $groups | Add-Member -MemberType NoteProperty -Value $Identity -Name "User"

        # set type for all objects
        $groups | %{$_.pstypenames.Insert(0, "ETHZ.ID.IAMClient.IAMGroupMembership")}

        return ($groups | Sort-Object -property Type, Name)
        
    }

    END { }

}