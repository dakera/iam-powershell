function Get-ETHUser {
    <#
    .SYNOPSIS
        Gets the parameters of a IT-Service for a user (Similar to Get-ADUser)
        
    .DESCRIPTION
        Gets all parameters for a given service from IAM for the given user
        Default is the "Mailbox" service

    .PARAMETER Identity
        The username to find

    .PARAMETER Service
        The service name to get the parameters for

    .EXAMPLE
        Get-ETHUser aurels

    .EXAMPLE
        Get-ETHUser aurels -Service LDAP
    
    #>
    param (
        [Parameter(Position = 0, Mandatory = 1, ValueFromPipeline = $true)]
        [string]$Identity,
        [Parameter(Position = 1, Mandatory = 0)]
        [string]$Service = "Mailbox"
    )

    BEGIN {
        $url = "/usermgr/user/$Identity/service/$Service"

        # is client initialized?
        Test-IsIAMClientInitialized | Out-Null
    }
    
    PROCESS {
        $result = Invoke-IAMMethod -Url $url -Method Get -Credentials $script:IAMCreds
    }

    END {
        return $result
    }
    
}