# ETH User Cmdlets



## Cmdlets

This group contains the following cmdlets:

- [Add-ETHUserITService](/docs/Add-ETHUserITService.md)
 - [Get-ETHUser](/docs/Get-ETHUser.md)
 - [Get-ETHUserGroupMembership](/docs/Get-ETHUserGroupMembership.md)
 - [Reset-ETHUserPassword](/docs/Reset-ETHUserPassword.md)
 - [Set-ETHUser](/docs/Set-ETHUser.md)
 - [Set-ETHUserITService](/docs/Set-ETHUserITService.md)

