function Add-ETHMaillistMember {
    param (
        # List Name
        [Parameter(Position = 0, Mandatory = $true)]
        [string]$Identity,

        # Member to add or remove
        [Parameter(Position = 1, Mandatory = $true, ValueFromPipeline = $true)]
        [string[]]$Members
    )

    BEGIN {
        Test-IsIAMClientInitialized | Out-Null

        $Url = "/mailinglists/$Identity/members/add"
    }

    PROCESS {
        return (Invoke-IAMMethod -Url $Url -Body $Members -Method Put -Credentials $script:IAMCreds)
    }


}