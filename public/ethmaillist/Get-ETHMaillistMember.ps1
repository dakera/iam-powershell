function Get-ETHMaillistMember {
    [CmdletBinding()]
    param (
        # List Name
        [Parameter(Position = 0, Mandatory = $true, ValueFromPipeline = $true)]
        [string]$Identity
    )

    BEGIN {
    
        $membersCn = (Get-ETHMaillist -Identity $Identity).members

    }

    PROCESS {
        
        foreach ($member in $membersCn) {
            $Name = ($member -split ",OU=")[0] -replace "CN="
        
            # determine if the member is a user or a mailinglist
            if ($member -like "*OU=EthLists,*") {
                $objectClass = "group"
            }
            else {
                $objectClass = "user"
            }
            
            # create simple return object
            [pscustomobject]@{
                name              = $Name;
                objectClass       = $objectClass;
                distinguishedName = $membersCn; 
            }
        }
    }
}