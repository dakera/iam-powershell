# ETH Maillist Group



## Cmdlets

This group contains the following cmdlets:

- [Add-ETHMaillistMember](/docs/Add-ETHMaillistMember.md)
 - [Clear-ETHMaillistMember](/docs/Clear-ETHMaillistMember.md)
 - [Get-ETHMaillist](/docs/Get-ETHMaillist.md)
 - [Get-ETHMaillistMember](/docs/Get-ETHMaillistMember.md)
 - [Remove-ETHMaillist](/docs/Remove-ETHMaillist.md)
 - [Remove-ETHMaillistMember](/docs/Remove-ETHMaillistMember.md)
 - [Set-ETHMaillistMembers](/docs/Set-ETHMaillistMembers.md)
 - [Sync-ETHMaillistMember](/docs/Sync-ETHMaillistMember.md)

