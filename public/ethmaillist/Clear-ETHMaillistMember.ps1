function Clear-ETHMaillistMember {
    param (
        # List Name
        [Parameter(Position = 0, Mandatory = $true)]
        [string]$Identity
    )

    BEGIN {
        Test-IsIAMClientInitialized | Out-Null

        $Url = "/mailinglists/$Identity/members"
    }

    PROCESS {
        return (Invoke-IAMMethod -Url $Url -Method Delete -Credentials $script:IAMCreds)
    }
}