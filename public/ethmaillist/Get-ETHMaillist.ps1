function Get-ETHMaillist {
    param (
        # List Name
        [Parameter(Position = 0, Mandatory = $true)]
        [string]$Identity
    )

    BEGIN {
        Test-IsIAMClientInitialized | Out-Null

        $Url = "/mailinglists/$Identity"
    }

    PROCESS {
        return (Invoke-IAMMethod -Url $Url -Method Get -Credentials $script:IAMCreds)
    }
}