function Sync-ETHMaillistMember {
    <#
    .SYNOPSIS
        Synchronizes members mailinglists (DEPRECATED)
        
    .DESCRIPTION
        Sync-ETHMaillist members can be used to emulate nesting without actually nesting groups
        The members can be synchronized from other groups, f.ex OC-groups and no_oc-Groups

    .PARAMETER ReferenceLists
        The lists to get the members **from**

    .PARAMETER SyncList
        The destination list that will be **modified**

    .EXAMPLE
        Sync-ETHMaillistMember -ReferenceLists "biol-micro-test-list" -SyncList "biol-micro-list-aebi"

        This copies all members from the list "biol-micro-test-list" to the list "biol-micro-list-aebi"

    .EXAMPLE
        Sync-ETHMaillistMember -ReferenceLists "biol-micro-test-list","biol-isg-all" -SyncList "biol-micro-list-aebi"

        This does the same as example 1, but with multiple sourcelists which will be combined
    
    .LINK
        Set-ETHMaillistMember
        Set-ETHGroupMember
        Sync-ETHGroupMember        
        
#>

    [CmdletBinding()]
    param(
        # Group Name
        [Parameter(Position = 0, Mandatory = $true)]
        [string[]]$ReferenceLists,

        # Members to sync to
        [Parameter(Position = 1, Mandatory = $true)]
        [string]$SyncList,

        # Falls back to AD if group cannot be loaded via IAM
        [Parameter(Position = 2)]
        [switch]$AllowADFallback
    )

    BEGIN {
        # Check if client is initialized
        Test-IsIAMClientInitialized | Out-Null

        # Validate destination list exists
        try {
            $null = Get-ETHMaillist $SyncGroup # discard output
        }
        catch {
            throw "Could not find maillist $ReferenceGroup or $SyncGroup"
        }
    }

    PROCESS {
        # Validate both groups exist
        $AllMembersList = @()
        
        foreach ($RefList in $ReferenceLists) {
            try {
                $List = Get-ETHMaillist $RefList 
                $AllMembersList += $List.members
            }
            catch {
                # Perform fallback if needed
                if (-not $AllowADFallback) {
                    throw "Could not find group $RefList in IAM"
                }
                try {
                    # get all users from AD group as fallback
                    $Members = Get-ADGroupMember -Identity $RefList | Where-Object objectClass -eq "user"
                    $AllMembersList += $Members.name
                }
                catch {
                    throw "Could not find group $RefList in AD"
                }
            
            }
        }

        Write-Debug "New Members: $AllMembersList"
        return (Set-ETHMaillistMembers -Identity $SyncGroup -Members $AllMembersList)
    }
}