function Remove-ETHMaillist {
    [CmdletBinding(SupportsShouldProcess = $true)]
    param (
        # List Name
        [Parameter(Position = 0, ParameterSetName = "ByName")]
        [string]$Identity,

        [Parameter(ValueFromPipeline = $true, ParameterSetName = "ByPipeline")]
        [psobject]$MailObject

    )

    BEGIN {
        Test-IsIAMClientInitialized | Out-Null

        if ($MailObject -ne $null) {
            $Identity = $MailObject.listName
        }

        $Url = "/mailinglists/$Identity"
    }

    PROCESS {
        if ($PSCmdlet.ShouldProcess("Deleting Mailliglist $Identity")) {
            return (Invoke-IAMMethod -Url $Url -Method Delete -Credentials $script:IAMCreds)
        }
    }
}