function Set-ETHMaillistMembers {
    [CmdletBinding()]
    param (
        # Maillist name
        [Parameter(Position = 0, Mandatory = $true)]
        [string]
        $Identity,

        # Members
        [Parameter(Position = 1, Mandatory = $true)]
        [string[]]$Members
    )

    BEGIN {
        Test-IsIAMClientInitialized | Out-Null

        try {
            $ExistingMembers = @((Get-ETHMaillistMember -Identity $Identity).name)
        }
        catch {
            throw "Could not find Mailinglist $Identity"
        }
    }

    PROCESS {
        $MemberCompare = Compare-GroupMembers -ExistingMembers $ExistingMembers -NewMembers $Members
        $ToAddMembers = $MemberCompare.ToAdd
        $ToRemoveMembers = $MemberCompare.ToRemove

        # Add members
        try {
            if ($ToAddMembers.Count -gt 0){
                $null = Add-ETHMaillistMember -Identity $Identity -Members $ToAddMembers
                Write-Debug "Successfully added $($ToAddMembers.Count) new members to Mailinglist $Identity"
            }
        }
        catch {
            Write-Error "Failed to add $($ToAddMembers.Count) members to mailinglist. $([System.Environment]::NewLine)Error: $_"
            return
        }

        # Remove members
        try {
            if ($ToRemoveMembers.Count -gt 0){
                $null = Remove-ETHMaillistMember -Identity $Identity -Members $ToRemoveMembers
                Write-Debug "Successfully removed $($ToRemoveMembers.Count) members from Mailinglist $Identity"
            }
        }
        catch {
            Write-Error "Failed to remove $($ToRemoveMembers.Count) members from mailinglist. $([System.Environment]::NewLine)Error: $_"
            return
        }

        return @{
            Added = $ToAddMembers;
            Removed = $ToRemoveMembers;
            Kept = $MemberCompare.ToKeep;
        }
    }
}