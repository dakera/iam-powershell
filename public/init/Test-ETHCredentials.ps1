function Test-ETHCredentials {
    [CmdletBinding()]
    param (
        [Parameter(Position = 0, Mandatory = 1)]
        [pscredential]$Credentials
    )
    try {
        $script:IAMCreds = $Credentials
        Get-ETHUser -Identity $Credentials.UserName
        $script:IAMCreds = $null
        return $true
    }
    catch {
        return $false
    }

}