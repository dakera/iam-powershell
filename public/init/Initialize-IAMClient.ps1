function Initialize-IAMClient {
    [CmdletBinding()]
    param(
        # Credentials to validate
        [Parameter(Position = 1, Mandatory = 1)]
        [pscredential]
        $Credentials,

        [switch]$Force
    )

    if ($null -ne $script:IAMCreds -and $Force -eq $false) {
        Write-Information "The Client is already initialized, use the -Force Switch to override credentials"
        return
    }

    if (-not (Test-ETHCredentials $Credentials)) {
        throw "Could not validate your credentials"
    }

    # Enable Debug mode for script
    if ($PSBoundParameters.Debug.IsPresent) {
        $script:DebugMode = $true
        $DebugPreference = "Continue" 
    }
    else {
        $DebugPreference = "SilentlyContinue"
    }

    $script:IAMCreds = $Credentials

    Set-StrictMode -Version latest
}
