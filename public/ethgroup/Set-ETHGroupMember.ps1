function Set-ETHGroupMember {
    <#
    .SYNOPSIS
        Sets the members of an ETH group to the specified member list
        
    .DESCRIPTION
        Removes / Adds members to the given group until the memberlist is equal to the one submitted
        You can specify either usernames or *custom* groups

    .PARAMETER Identity
        The group to edit

    .PARAMETER Members
        The list of members to set the group memberlist to

    .EXAMPLE
        PS> Set-ETHGroupMember -Identity biol-micro-isg -Members @("aurels","ausc")

    .EXAMPLE 
        PS> Set-ETHGroupMember -Identity biol-micro-isg -Members @("biol-micro-isg-sadm","aurels")

        Added:   {"aurels", "ausc"}
        Removed: {}
        Kept:    {}

    .OUTPUTS
        pscustomobject. Returns a custom object with 3 properties Added, Removed and Kept to show what the cmdlet did

    #>
    [CmdletBinding(SupportsShouldProcess=$true,HelpUri="https://gitlab.ethz.ch/aurels/iam-powershell/tree/master/docs/Set-ETHGroupMember.md")]
    [OutputType([PSCustomObject])]
    param(
        # Group Name
        [Parameter(Position = 0, Mandatory = $true)]
        [string]$Identity,

        # Members to sync to
        [Parameter(Position = 1, Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = "Name")]
        [AllowEmptyCollection()]
        [string[]]
        $Members
    )

    BEGIN {
        # Check if client is initialized
        Test-IsIAMClientInitialized | Out-Null

        try {
            $ExistingMembers = @(Get-ETHGroupMember $Identity)
        }
        catch {
            throw "Could not find group $Identity"
        }
    }

    PROCESS {
        # get members to be removed & added
        $MemberCompare = Compare-GroupMembers -ExistingMembers $ExistingMembers -NewMembers $Members
        $ToBeAdded = $MemberCompare.ToAdd
        $ToBeRemoved = $MemberCompare.ToRemove

        try {

            if ($ToBeAdded.Count -gt 0) {
                if ($PSCmdlet.ShouldProcess($Identity, "Add-ETHGroupMember")) {
                    # Discard output of Add-ETHGroupMember
                    $null = Add-ETHGroupMember -Identity $Identity -Members $ToBeAdded
                }
            }

            if ($ToBeRemoved.Count -gt 0) {
                if ($PSCmdlet.ShouldProcess($Identity, "Remove-ETHGroupMember")) {
                    $null = Remove-ETHGroupMember -Identity $Identity -Members $ToBeRemoved
                }
            }

        }
        catch {
            throw "Failed to update group membership of group $Identity, try again to restore group integrity!`r`nError: $_"
        }

        return @{
            Added = $ToBeAdded;
            Removed = $ToBeRemoved;
            Kept = $MemberCompare.ToKeep;
        }
        
    }

    END { }
}