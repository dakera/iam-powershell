function Add-ETHGroupMember {
    [CmdletBinding(SupportsShouldProcess = 1)]
    param (   
        [Parameter(Position = 0, Mandatory = 1, ValueFromPipeline = 1)]
        [string]$Identity,
        [string[]]$Members
    )

    BEGIN {
        # Check if client is initialized
        Test-IsIAMClientInitialized | Out-Null

        # Validate input arguments
        if (-not ($ExistingGroup = Get-ETHGroup -Identity $Identity)) {
            throw "Group $Identity was not found"
        }

        if ($Members.Count -le 0) {
            throw "No members specified"
        }

        $ToAddMembers = @($Members | Where-Object { $ExistingGroup.members -notcontains $_ })

        if ($ToAddMembers.Count -eq 0) {
            Write-Debug "No new members added to group $Identity"
            return
        }
    }

    PROCESS {
        if ($PSCmdlet.ShouldProcess($Identity)) {
            try {
                $result = Invoke-IAMMethod -Url "/groupmgr/group/$Identity/members/add" -Method Put -Body $ToAddMembers -Credentials $script:IAMCreds
            }
            catch {
                throw "Could not update group $Identity, Error: $_"
                return
            }
        }
    }

    END {
        Write-Debug "Added $($ToAddMembers.Count) Members to Group $Identity"
        return $result
    }
    
}
