function Get-ETHGroupMember {
    [CmdletBinding()]
    param (
        [Parameter(Position = 0, Mandatory = 1, ValueFromPipelineByPropertyName = 1)]
        [string]$Identity
    )

    BEGIN {
        $Group = Get-ETHGroup -Identity $Identity
    }

    PROCESS {
        return ($Group | Select-Object -expand members)
    }

    END {

    }

}