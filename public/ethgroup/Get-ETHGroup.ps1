function Get-ETHGroup {
    param (
        [CmdletBinding()]
        [Parameter(Position = 0, Mandatory = 1)]
        [string]$Identity
    )

    BEGIN {
        # Check if client is initialized
        Test-IsIAMClientInitialized | Out-Null

        $url = "/groupmgr/group/$Identity"
    }

    PROCESS {
        return (Invoke-IAMMethod -Url $url -Method Get -Credentials $script:IAMCreds)
    }

    END {

    }    
}

