function Sync-ETHGroupMember {

    <#
    .SYNOPSIS
    Synchronizes users from multiple groups and mailing lists to a group and a mailinglist
    
    .DESCRIPTION
    Copies all **users** from the source groups/lists to the given destination group/list
    
    .PARAMETER SourceGroups
    The list of groups to read members from
    
    .PARAMETER SourceLists
    The list of mailinglists to read members from    
    
    .PARAMETER DestGroup
    The destination group that will be set to all members from the given groups / lists

    .PARAMETER DestList
    The destination mailinglist that will be set to all members from the given groups / lists

    .EXAMPLE
    PS> Sync-ETHGroupMember -SourceGroups "biol-micro-isg" -DestList "MICRO_IT_STAFF"

    Copies all members from the group "biol-micro-isg" to the Maillinglist "MICRO_IT_STAFF"
    
    .EXAMPLE
    PS> Sync-ETHGroupMember -SourceLists "MICRO_IT_STAFF","MICRO_AD_STAFF" -DestList "MICRO_STAFF"

    Copies all members from the source lists to the destination list

    .EXAMPLE
    PS> Sync-ETHGroupMember -SourceLists "MICRO_IT_STAFF","MICRO_AD_STAFF" -SourceGroups "biol-micro-institute" -DestGroup "biol-micro-institute"

    Adds all members from the given lists to the destination group without removing users

    .OUTPUTS
        System.Collections.Hashtable
        A hashtable with a report on each group/list that was modified and what was done (Add / Remove / Keep Members)
    
    #>


    [CmdletBinding(SupportsShouldProcess = $true, DefaultParameterSetName="ToGroup")]
    [OutputType([System.Collections.HashTable])]
    param(
        # Group Name
        [Parameter(Position = 0)]
        [Alias("ReferenceGroups")]
        [string[]]$SourceGroups,

        # Mailinglist to sync from
        [Parameter(Position = 1)]
        [string[]]$SourceLists,

        # Group to sync members to 
        [Parameter(Position = 2, ParameterSetName = "ToGroup", Mandatory = 1)]
        [Parameter(ParameterSetName = "ToBoth", Mandatory = 1)]
        [Alias("SyncGroup")]
        [string]$DestGroup,

        # List to sync members to 
        [Parameter(Position = 3, ParameterSetName = "ToList", Mandatory = 1)]
        [Parameter(ParameterSetName = "ToBoth", Mandatory = 1)]
        [string]$DestList,

        # Falls back to AD if group cannot be loaded via IAM
        [Parameter()]
        [switch]$AllowADFallback
    )

    BEGIN {

        # Validate input arguments
        if ($SourceGroups.Count -eq 0 -and $SourceLists.Count -eq 0) {
            throw "At least one source group or list has to be specified!"
        }

        if ([string]::IsNullOrWhiteSpace($DestGroup) -and [string]::IsNullOrWhiteSpace($DestList)) {
            throw "At least one destination group has to be specified!"
        }

        if ($AllowADFallback -and (Get-Module).Name -notcontains "ActiveDirectory") {
            try {
                Import-Module ActiveDirectory
            }
            catch {
                throw "To use the ActiveDirectory fallback, install RSAT tools!"
            }
        }

        # Check if client is initialized
        Test-IsIAMClientInitialized | Out-Null

        # Validate destination group exists
        if ($DestGroup) {
            try {
                $null = Get-ETHGroup -Identity $DestGroup # discard output
            }
            catch {
                throw "Could not find group $DestGroup"
            }
        }

        if ($DestList) {
            try {
                $null = Get-ETHMaillist -Identity $DestList # discard output
            }
            catch {
                throw "Could not find list $DestList"
            }
        }

        $ListsToProcess = @()
        $GroupsToProcess = @()

         if ($SourceLists.Count -gt 0) { 
            $ListsToProcess = @($SourceLists | ForEach-Object { [PSCustomObject]@{Name = $_; Type = "List" } }) 
        }

        if ($SourceGroups.Count -gt 0){
            $GroupsToProcess = @($SourceGroups | ForEach-Object { [PSCustomObject]@{Name = $_; Type = "Group" } })
        }
    }

    PROCESS {
        
        # Store all members from the different sourcegroups in a hashset, 
        # so that duplicates are automatically eliminated
        $AllMembersList = New-Object 'System.Collections.Generic.HashSet[string]' 
        
        foreach ($Source in @($ListsToProcess + $GroupsToProcess)) {
            # retrieve type for output messages
            $SourceType = $Source.Type
            try {
                switch ($SourceType) {
                    "Group" { 
                        # Get Group members
                        $Group = Get-ETHGroup $Source.Name
                        $AllMembersList.UnionWith([string[]]@($Group.members))
                    }
                    "List" {
                        # Get Maillist members
                        $ListMembers = Get-ETHMaillistMember $Source.Name | Where-Object objectClass -eq "user"
                        $AllMembersList.UnionWith([string[]]@($ListMembers.name)) # add all members to the list
                    }
                    Default {
                        # Invalid
                        throw "GroupType '$SourceType' invalid. Valid are 'List','Group'!"
                    }
                }
            
            }
            catch {
                # Group / List was not found in IAM
                # Perform ad fallback if needed
                if (-not $AllowADFallback) {
                    throw "Could not find $SourceType '$($Source.Name)' in IAM"
                }
                try {
                    # get all users from AD group as fallback
                    $Members = Get-ADGroupMember -Identity $Source.Name | Where-Object objectClass -eq "user"
                    $AllMembersList.UnionWith([string[]]($Members.name))
                }
                catch {
                    throw "Could not find $SourceType '$($Source.Name)' in AD"
                }
            
            }
        }

        # Store changes in a hashtable for every group modified
        $Changes = @{ }


        if ($DestGroup -ne "" -and $PSCmdlet.ShouldProcess($DestGroup, "Set-ETHGroupMember")) {
            $Changes.Add($DestGroup, (Set-ETHGroupMember -Identity $DestGroup -Members $AllMembersList))
        }

        if ($DestList -ne "" -and $PSCmdlet.ShouldProcess("$DestList", "Set-ETHMaillistMembers")) {
            $Changes.Add($DestList, (Set-ETHMaillistMembers -Identity $DestList -Members $AllMembersList))
        }

        return $Changes
    }

    END {

    }
}
