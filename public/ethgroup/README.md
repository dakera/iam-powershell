# ETH Group Cmdlets



## Cmdlets

This group contains the following cmdlets:

- [Add-ETHGroupmember](/docs/Add-ETHGroupmember.md)
 - [Get-ETHGroup](/docs/Get-ETHGroup.md)
 - [Get-ETHGroupMember](/docs/Get-ETHGroupMember.md)
 - [Remove-ETHGroupMember](/docs/Remove-ETHGroupMember.md)
 - [Set-ETHGroupMember](/docs/Set-ETHGroupMember.md)
 - [Sync-ETHGroupMember](/docs/Sync-ETHGroupMember.md)

