

function Remove-ETHGroupMember {
    [CmdletBinding(SupportsShouldProcess = $true)]
    param (
        [Parameter(Position = 0, Mandatory = 1)]
        [string]$Identity,

        [Parameter(Position = 1, Mandatory = 1)]
        [string[]]$Members
    )

    BEGIN {
        # Check if client is initialized
        Test-IsIAMClientInitialized | Out-Null

        # Validate input arguments
        if (-not ($ExistingGroup = Get-ETHGroup -Identity $Identity)) {
            throw "Group $Identity was not found"
        }

        if ($Members.Count -le 0) {
            throw "No members specified"
        }
    }

    PROCESS {
        $ToRemoveMembers = @($Members | Where-Object { $ExistingGroup.members -contains $_ })

        # Check if there are any members in the group that need to be removed
        if ($ToRemoveMembers.Count -eq 0) {
            Write-Debug "Did not need to remove any members from $Identity"
            return $ExistingGroup
        }

        if ($PSCmdlet.ShouldProcess($Identity)) {
            try {
                return (Invoke-IAMMethod -Url "/groupmgr/group/$Identity/members/del" -Method Put -Body $ToRemoveMembers -Credentials $script:IAMCreds)
            }
            catch {
                throw "Could not update group $Identity"
            }
        }
    }

    END {}
}