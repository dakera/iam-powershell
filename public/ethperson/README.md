# Person Cmdlets



## Cmdlets

This group contains the following cmdlets:

- [Get-ETHPerson](/docs/Get-ETHPerson.md)
 - [Get-ETHPersonServices](/docs/Get-ETHPersonServices.md)
 - [New-ETHPersona](/docs/New-ETHPersona.md)

