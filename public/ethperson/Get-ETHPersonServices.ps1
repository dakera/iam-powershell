function Get-ETHPersonServices {
    param (
        # ETH user name
        [Parameter(Position = 0, Mandatory = 1)]
        [String]$Identity
    )

    BEGIN {
        # Check if client is initialized
        Test-IsIAMClientInitialized | Out-Null
    }

    PROCESS {
        return (Invoke-IAMMethod -Url "/usermgr/user/$Identity/services" -Method GET -Credentials $script:IAMCreds)
    }
}