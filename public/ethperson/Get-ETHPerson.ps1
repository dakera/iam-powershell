function Get-ETHPerson {
    param (
        [Parameter(Position = 0, Mandatory = 1)]
        [string]$Identity
    )

    BEGIN {
        $url = "/usermgr/person/$Identity"

        # is client initialized?
        Test-IsIAMClientInitialized | Out-Null
    }

    PROCESS {
        return (Invoke-IAMMethod -Url $url -Method Get -Credentials $script:IAMCreds)
    }

    END {

    }
    
}