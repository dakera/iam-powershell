function New-ETHPersona {
    param(
        # Username of Persona parent
        [Parameter(Position = 0, Mandatory = 1)]
        [string]
        $ParentIdentity,

        # New Username
        [Parameter(Position = 1, Mandatory = 1)]
        [string]
        $NewUserName,

        [Parameter(Position = 2)]
        [string]
        $UserComment = "",

        # Initial Password
        [Parameter(Position = 3, Mandatory = 1)]
        [string]
        $InitPwd

    )

    BEGIN {
        $NewUser = [PSCustomObject]@{
            username    = $NewUserName;
            memo        = $UserComment;
            init_passwd = $NewPassword;
        }
    }

    PROCESS {
        Invoke-IAMMethod -Url "/usermgr/person/$ParentIdentity" -Method POST -Credentials $script:IAMCreds -Body $NewUser
    }
}