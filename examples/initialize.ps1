Set-Location $PSScriptRoot

if (-not $global:AdminUser){
    $global:AdminUser = Get-Credential
}

Import-Module ..\IAMClient.psd1 -Force

$DebugPreference = "Continue"

Initialize-IAMClient $AdminUser -Debug

