function ToHtmlList {
    param([string[]]$Entries, [string]$Prefix="")
    if ($null -eq $Entries) {return}
    $ul = "<ul>"
    $Entries | % {$ul += "<li>$Prefix $_</li>"}
    $ul += "</ul>"
    return $ul
}


$Mail = "aurels@micro.biol.ethz.ch" 
$PSEmailServer = "smtp0.ethz.ch"
$ErrorActionPreference = "Continue"
$Start = Get-Date

.\initialize.ps1

$GroupSync = @(
    # Piel
    @{DestList = "biol-micro-list-piel";  SourceGroups = "biol-micro-piel-no_oc","03980_lz"},
    @{DestGroup="biol-micro-piel";        SourceLists = "biol-micro-list-piel";        SourceGroups = "biol-micro-piel-tech-accounts"},

    # Vorholt
    @{DestList = "biol-micro-list-vorholt"; SourceGroups = "biol-micro-vorholt-no_oc","08818_lz","03740_lz"},
    @{DestGroup="biol-micro-vorholt";       SourceLists = "biol-micro-list-vorholt";   SourceGroups = "biol-micro-vorholt-tech-accounts"},
    
    # Hardt
    @{DestList = "biol-micro-list-hardt"; SourceGroups = "biol-micro-hardt-no_oc","03589_lz"},
    @{DestGroup="biol-micro-hardt";       SourceLists = "biol-micro-list-hardt";       SourceGroups = "biol-micro-hardt-tech-accounts"},
        
    # Sallusto
    @{DestList = "biol-micro-list-sallusto"; SourceGroups = "biol-micro-sallusto-no_oc","09604_lz"},
    @{DestGroup="biol-micro-sallusto";       SourceLists = "biol-micro-list-sallusto"; SourceGroups = "biol-micro-sallusto-tech-accounts"},
    
    # Künzler
    @{DestList = "biol-micro-list-kuenzler"; SourceGroups = "biol-micro-kuenzler-no_oc","08838_lz"},
    @{DestGroup="biol-micro-kuenzler";       SourceLists = "biol-micro-list-kuenzler"; SourceGroups = "biol-micro-kuenzler-tech-accounts"},
    
    # Aebi
    @{DestList = "biol-micro-list-aebi"; SourceGroups = "biol-micro-aebi-no_oc","03408_lz"; SourceLists = "biol-micro-list-kuenzler"},
    @{DestGroup="biol-micro-aebi";       SourceLists = "biol-micro-list-aebi";         SourceGroups = "biol-micro-kuenzler","biol-micro-aebi-tech-accounts"},
    
    # Sunagawa
    @{DestList = "biol-micro-list-sunagawa"; SourceGroups = "biol-micro-sunagawa-no_oc","09583_lz"},
    @{DestGroup="biol-micro-sunagawa";       SourceLists = "biol-micro-list-sunagawa"; SourceGroups = "biol-micro-sunagawa-tech-accounts"},
    
    # Oxenius
    @{DestList = "biol-micro-list-oxenius";  SourceGroups = "biol-micro-oxenius-no_oc","03625_lz"}
    @{DestGroup="biol-micro-oxenius";        SourceLists = "biol-micro-list-oxenius";  SourceGroups = "biol-micro-oxenius-tech-accounts"}
    
)

$changes = ""
$errors = @()

# Sync all Groups
foreach ($Group in $GroupSync){

    # Use DestGroup as title, and if nothing is set, use list
    $GroupTitle = if($Group.DestGroup){$Group.DestGroup} else {$Group.DestList}

    Write-Host $GroupTitle
    
    $changesMade = $false

    $thisGroupChanges = "<h1 style='font-size: 15pt'>$GroupTitle</h1>"

    if ($Group.SourceGroups -or $Group.SourceLists){
        $thisGroupChanges += "<p>Source:</p>"
        $thisGroupChanges += ToHtmlList $Group.SourceGroups -Prefix "Group:"
        $thisGroupChanges += ToHtmlList $Group.SourceLists -Prefix "List:"
    }

    try {
        # Sync Group with parameters from config
        $GroupResult = Sync-ETHGroupMember @Group -AllowADFallback
    } catch {
        $errors += $_
        continue
    }

    # "Pretty" print results
    foreach ($Result in $GroupResult.Keys){

        if ($GroupResult.Keys.Count -gt 1){
            $thisGroupChanges += "<h2>$($Result)</h2>"
        }

        if (($GroupResult[$Result].Added.Count + $GroupResult[$Result].Removed.Count) -gt 0){
            $thisGroupChanges += "<p>Modified Members:</p>"
            $thisGroupChanges += ToHtmlList $GroupResult[$Result].Added -Prefix "+"
            $thisGroupChanges += ToHtmlList $GroupResult[$Result].Removed -Prefix "-"

            $changesMade = $true
        }
    }

    # only put Group output in E-Mail body if changes have been made
    if ($changesMade){
        $changes += $thisGroupChanges
    }
}

# Output errors if needed
if ($errors.Count -gt 1){
    $changes += "The following Errors occured:"
    $changes += ToHtmlList $errors
}

# compute execution time
$Finish = Get-Date
$Duration = $Finish - $Start

if ($changes -ne ""){
    $changes += "<p>Execution time: $([math]::Round($Duration.TotalMinutes, 2))m</p>"
    Send-MailMessage -BodyAsHtml -Body $changes -To $Mail -From "IAM Group Sync <hyde@micro.biol.ethz.ch>" -Subject "Sync Report $($Finish.ToString('dd.MM.yyyy HH:mm'))"
}