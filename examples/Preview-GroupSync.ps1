# This file creates reports to compare what the Sync-ETHGroupMember CmdLet would do

$Groups = @(
    @{Name ="biol-micro-aebi"; LZ="03408_lz"},
    @{Name ="biol-micro-hardt"; LZ="03589_lz"},
    @{Name ="biol-micro-oxenius"; LZ="03625_lz"},
    @{Name ="biol-micro-sunagawa"; LZ="09583_lz"},
    @{Name ="biol-micro-vorholt"; LZ="03740_lz"},
    @{Name ="biol-micro-piel"; LZ="03980_lz"},
    @{Name ="biol-micro-sallusto"; LZ="09604_lz"}
)

foreach ($Group in $Groups){
    $Members = Get-ADGroupMember $Group.Name
    $LZ = Get-ADGroupMember $Group.LZ

    Write-Host -f Cyan $Group.Name

    Compare-Object $Members.Name $LZ.Name
}
