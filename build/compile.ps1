Set-Location $PSScriptRoot

$ErrorActionPreference = "Stop"

$Private = @(Get-ChildItem -Recurse -File -Include "*.ps1" "../private/")
$Public = @(Get-ChildItem -Recurse -File -Include "*.ps1" "../public/")
$Script = "IAMClient.psm1"
$PsdFile = "IAMClient.psd1"
$OutPath = "../dist"
$ToCopy = @("../$PsdFile","../IAMClient.Format.ps1xml")

# read version specified in .psd1 file
$PsdFileContent = Invoke-Expression -Command (Get-Content "..\$PsdFile" -Raw)
$ModuleVersion = $PsdFileContent.ModuleVersion

if (Test-Path $OutPath){
    Remove-Item $OutPath -Force -Recurse
}

# Create dist directory
New-Item -ItemType Directory $OutPath
$ScriptPath = (New-Item -ItemType File -Path $OutPath -Name $Script).FullName

$AllCmdlets = @($Private + $Public) | sort -Property basename

# Copy all cmdlets to the output module file
foreach ($File in $AllCmdlets){
    # Store content of file in script
    Get-Content $File | Out-File $ScriptPath -Append
    [System.Environment]::NewLine | Out-File $ScriptPath -Append
}

# Export all publich functions
$Functions = ($Public.basename | ForEach-Object {"'$_'"}) -join ","
"Export-ModuleMember -Function " + $Functions | Out-File $ScriptPath -Append

# copy all additional files
foreach ($Item in $ToCopy){
    Copy-Item -path $Item -Destination $OutPath -Recurse
}


$PsdPath = "$OutPath/$PsdFile"
# Save exported functions to manifest file
Update-ModuleManifest -Path $PsdPath -FunctionsToExport $Public.basename

# create zip archive of dist folder and copy to dist folder
Compress-Archive -LiteralPath (Get-ChildItem $OutPath).FullName -DestinationPath "$OutPath/IAMClient-$ModuleVersion.zip"