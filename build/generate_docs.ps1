Set-Location $PSScriptRoot

Import-Module platyPS -ErrorAction Stop

if (Get-Module IAMClient){
    Remove-Module IAMClient
}
Import-Module "..\IAMClient.psd1" -Force

if ((Get-ChildItem ..\docs).Count -gt 0) {
    Update-MarkdownHelpModule -Path ..\docs -RefreshModulePage -ModulePagePath ..\docs\README.md
    Update-MarkdownHelp -Path ..\docs -Force
} else {
    New-MarkdownHelp -Module IAMClient -OutputFolder "..\docs" -WithModulePage -ModulePagePath "..\docs\README.md" -Force
}

$newLine = [System.Environment]::NewLine

# Generate README.md files in the directories
$PublicGroups = Get-ChildItem -path (Join-Path -Path ".." "public") -Directory
$ReadmeFile = "README.md"

foreach ($Group in $PublicGroups) {
    
    $GroupPath = $Group.FullName

    $Scripts = Get-ChildItem -Path $GroupPath -Filter "*.ps1"

    $ReadmeFileContent = get-content (Join-Path $GroupPath $ReadmeFile) -Raw
    $ReadmeFileContent = ($ReadmeFileContent -split "## Cmdlets")[0]
    $CmdLetListing = ($Scripts.basename | ForEach-Object {"- [$_](/docs/$_.md)$newLine"})
    $ReadmeFileContent + `
        "## Cmdlets" + $newLine + $newLine + `
        "This group contains the following cmdlets:" + $newLine + $newLine + `
        $CmdLetListing | Out-File (Join-Path $GroupPath "README.md") -Encoding utf8

}