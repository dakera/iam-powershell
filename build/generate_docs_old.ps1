function FenceCodeBlock {
    param([string]$Content, [string]$Lang = "powershell")
    return '```' + $Lang + [System.Environment]::NewLine + $Content + [System.Environment]::NewLine + '```' 
}

Set-Location $PSScriptRoot

import-module "..\IAMClient.psm1" -Force

$PublicGroups = Get-ChildItem -Directory "..\public"
$DirSeparator = [System.IO.Path]::DirectorySeparatorChar
$newLine = [System.Environment]::NewLine
$ReadmeFile = "README.md"

# Docs folder setup
$DocsFolder = "..\docs"
# Cleanup if it exists
if (test-path $DocsFolder ) {
    Remove-Item $DocsFolder  -Recurse -Force
} 

# Create docs folder
mkdir $DocsFolder | Out-Null

foreach ($Group in $PublicGroups) {
    
    $GroupPath = $Group.FullName

    $GroupDocsPath = (mkdir ($DocsFolder + $DirSeparator + $Group.Name)).FullName

    $Scripts = Get-ChildItem -Path $Group.FullName -Filter "*.ps1"

    $ReadmeFileContent = get-content ($GroupPath + $DirSeparator + $ReadmeFile) -Raw
    $ReadmeFileContent = ($ReadmeFileContent -split "## Cmdlets")[0]
    $CmdLetListing = $newLine + ($Scripts.basename | ForEach-Object {"- [$_](/docs/$_.auto.md)$newLine"})
    $ReadmeFileContent + $newLine + `
        "## Cmdlets" + $newLine + $newLine + `
        "This group contains the following cmdlets:" + $newLine + $newLine + `
        $CmdLetListing | Out-File ($GroupPath + $DirSeparator + "README.md") -Encoding utf8

    foreach ($Script in $Scripts) {
        $CmdLetName = $Script.BaseName
        Write-Host "Generating docs for $CmdLetName"

        $MdFile = $GroupDocsPath + $DirSeparator + "$CmdLetName.auto.md"

        try {
            $CmdLetHelp = Get-Help $CmdLetName -Full -ErrorAction Stop
        }
        catch {
            # help not found
            Write-Host -f Red "Could not find help for $CmdLetName"
            continue
        }

        # CmdLet Title
        "# " + $CmdLetName + $newLine | Out-File $MdFile -Append

        # Synopsis
        $CmdLetHelp.Synopsis + $newLine | Out-File $MdFile -Append

        # Syntax
        foreach ($Syntax in $CmdLetHelp.Syntax.syntaxItem) {
            $newLine + (FenceCodeBlock -Content $Syntax) + $newLine | Out-File $MdFile -Append
        }
        
        # Description
        '## DESCRIPTION' + $newLine | Out-File $MdFile -Append
        $CmdLetHelp.description + $newLine | Out-File $MdFile -Append

        # Examples
        "## EXAMPLES" + $newLine | Out-File $MdFile -Append
        foreach ($example in $CmdLetHelp.examples.example) {
            "### " + $example.title + $newLine | Out-File $MdFile -Append
            (FenceCodeBlock -Content $example.code) + $newLine | Out-File $MdFile -Append
        }

        # Parameters
        "## PARAMETERS" + $newLine | Out-File $MdFile -Append
        foreach ($parameter in $CmdLetHelp.parameters.parameter) {
            "#### -" + $parameter.name + $newLine | Out-File $MdFile -Append
            $parameter.Description + $newLine | Out-File $MdFile -Append
            (FenceCodeBlock -Content ($parameter | Out-String) -Lang yaml) | Out-File $MdFile -Append
        }

        # Simple addition titles
        $AdditionalTitles = @("INPUTS", "OUTPUTS", "NOTES", "RELATED LINKS")
        foreach ($additionalTitle in $AdditionalTitles) {
            '## ' + $additionalTitle + $newLine | Out-File $MdFile -Append
            $CmdLetHelp.($additionalTitle) + $newLine | Out-File $MdFile -Append
        }
    }
}