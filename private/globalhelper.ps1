Function Write-RequestToConsole {
    param (
        [string]$Method,
        [hashtable]$Headers,
        [string]$JsonBody
    )

    Write-Debug "-- || -- || -- || -- || --"

    Write-Debug "$($Method.toUpper()) $Uri"

    foreach ($h in $Headers.Keys) {
        # Do not print basic auth string to console, instead override with some value
        if ($h -ne "Authorization") {
            Write-Debug "${h}: $($Headers[$h])"
        }
        else {
            Write-Debug "${h}: Basic BasicAuthString99999="
        }
    }
    Write-Debug "Body: $JsonBody"
    
}

function Write-ResponseToConsole {
    param (
        $Response
    )
    Write-Debug "------ RESPONSE:" 
    if ($null -ne $Response) {
        Write-Debug (ConvertTo-Json $Response)
    }
}

function Convert-CnToName {
    [CmdletBinding()]
    param(
        # Canonical Name
        [Parameter(Position = 0, Mandatory = $true, ValueFromPipeline = $true)]
        [string]$Cn
    )

    PROCESS {
        return ($Cn -split ",")[0] -replace "CN="
    }
}