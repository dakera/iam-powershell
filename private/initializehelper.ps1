function Test-IsIAMClientInitialized {
    if ($null -eq $script:IAMCreds) {
        throw "Please initialize the client to use this function"
        return $false
    }

    return $true
}