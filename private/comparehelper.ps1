Function Compare-ObjectProperties {
    Param(
        [PSObject]$ReferenceObject,
        [PSObject]$DifferenceObject 
    )
    $objprops = $ReferenceObject | Get-Member -MemberType Property, NoteProperty | Select-Object -expand  Name
    $objprops += $DifferenceObject | Get-Member -MemberType Property, NoteProperty | Select-Object -expand Name
    $objprops = $objprops | Sort-Object -Unique
    $diffs = @()
    foreach ($objprop in $objprops) {
        $diff = Compare-Object $ReferenceObject $DifferenceObject -Property $objprop
        if ($diff) {            
            $diffprops = @{
                PropertyName = $objprop
                RefValue     = ($diff | Where-Object { $_.SideIndicator -eq '<=' } | Foreach-Object $($objprop))
                DiffValue    = ($diff | Where-Object { $_.SideIndicator -eq '=>' } | Foreach-Object $($objprop))
            }
            $diffs += New-Object PSObject -Property $diffprops
        }        
    }
    if ($diffs) { return ($diffs | Select-Object PropertyName, RefValue, DiffValue) }     
}

Function Get-ObjectDiffs {
    param (
        [PSObject]$ReferenceObject,
        [PSObject]$DifferenceObject
    )

    $ChangedProps = @{ }

    Compare-ObjectProperties $ReferenceObject $DifferenceObject | ForEach-Object {
        $ChangedProps.Add($_.PropertyName, $_.DiffValue);
    }

    return $ChangedProps
}

function Compare-GroupMembers {

    param (
        [string[]]$ExistingMembers,
        [string[]]$NewMembers
    )

    BEGIN {
        $ToAddMembers = @()
        $ToRemoveMembers = @()
        $ToKeepMembers = @()
    }

    PROCESS {
        if ($ExistingMembers.Count -eq 0) {
            # No existing members -> add all
            $ToAddMembers = $NewMembers
        }
        elseif ($NewMembers.Count -eq 0) {
            # No new members -> remove all existing
            $ToRemoveMembers = $ExistingMembers
        }
        else {
            # everything fine, we can run compare-object
            $ComparisionResult = Compare-Object -ReferenceObject $ExistingMembers -DifferenceObject $Members -IncludeEqual

            $ToAddMembers = ($ComparisionResult | Where-Object SideIndicator -eq "=>").InputObject
            $ToRemoveMembers = ($ComparisionResult | Where-Object SideIndicator -eq "<=").InputObject
            $ToKeepMembers = ($ComparisionResult | Where-Object SideIndicator -eq "==").InputObject
        }
    
        return [PSCustomObject]@{
            ToAdd    = $ToAddMembers;
            ToRemove = $ToRemoveMembers;
            ToKeep   = $ToKeepMembers;
        }

    }
}
